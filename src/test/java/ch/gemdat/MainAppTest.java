package ch.gemdat;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import org.hamcrest.core.StringContains;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import ch.gemdat.domain.Person;
import ch.gemdat.repository.PersonRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MainApp.class)
@WebAppConfiguration
public class MainAppTest {

    private MockMvc mockMvc;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();

        this.personRepository.deleteAll();
        Person test= new Person();
        test.setName("Testy");
        this.personRepository.save(test);
    }

    @Test
    public void userNotFound() throws Exception {
        mockMvc.perform(get("/get?name=Mark"))
        		 .andExpect(status().isOk());
                
    }
    
    @Test
    public void userFound() throws Exception {
        mockMvc.perform(get("/get?name=Testy"))
        		 .andExpect(status().isOk())
        		 .andExpect(content().string("Testy"));
                
    }
    
    @Test
    public void saveUser() throws Exception {
        mockMvc.perform(get("/save?name=saveTest"))
        		 .andExpect(status().isOk())
        		 .andExpect(content().string(StringContains.containsString("saveTest")));
                
    }
    
    @Test
    public void reverseUser() throws Exception {
        mockMvc.perform(get("/reverse?name=saveTest"))
        		 .andExpect(status().isOk())
        		 .andExpect(content().string("tseTevas"));
                
    }
}