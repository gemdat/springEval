package ch.gemdat.repository;

import java.util.List;

import org.springframework.data.domain.*;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.*;

import ch.gemdat.domain.*;

public interface PersonRepository extends CrudRepository<Person, Long> {

    Page<Person> findAll(Pageable pageable);

    Person findByNameAllIgnoringCase(String name, Long id);
    
    @Query("FROM Person WHERE name like %?1%")
    List<Person> gimmeThisPerson(String name);
}