package ch.gemdat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ch.gemdat.domain.Person;
import ch.gemdat.repository.PersonRepository;

@RestController
@EnableAutoConfiguration
public class MainApp {
	
	@Autowired
	private PersonRepository repository;

	public static void main(String[] args) {
		SpringApplication.run(MainApp.class, args);
	}
	
	@RequestMapping("/")
	String home() {
		return "Hello Spring-boot!";
	}

	@RequestMapping("/reverse")
	String reverse(@RequestParam(value="name") String name) {

		return new StringBuilder(name).reverse().toString();
	}
	
	@RequestMapping("/save")
	String save(@RequestParam(value="name") String name) {
		Person person= new Person();
		person.setName(name);
		Person savedPerson= repository.save(person);
		return savedPerson.getName() + " " + savedPerson.getId() ;
	}
		
	@RequestMapping("/get")
	String get(@RequestParam(value="name") String name) {
		List<Person> persons= repository.gimmeThisPerson(name);
		StringBuilder result = new StringBuilder();
		persons.forEach(p -> result.append(p.getName()));
		return result.toString();
	}
	
	
	
}