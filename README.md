# springEval
Evaluation Project with spring boot and flyway migrations and gradle.


<h3>Benötigt:</h3>
<ul>
  <li>Eclipse Mars <br><link>https://projects.eclipse.org/releases/mars</link></li>
  <li>Gradle 2.6 <br><link>https://gradle.org/gradle-nightly-build/</link></li>
  <li>Jdk 8 <br><link>http://www.oracle.com/technetwork/java/javase/downloads/</link></li>
  <li>Postgres 9.4 <br><link>http://www.postgresql.org/download/</link></li>
</ul>

<h3>Gradle</h3>
Tasks auflisten:<br>
<code>gradle task</code>

<h3>FlyWay</h3>
DB initialisieren:<br>
<code>gradle flywayBaseline</code> (setzt den initialen Migrations-Stand der DB)<br>
<code>gradle flywayMigrate</code> (alle Migrationscripts ausführen ab der Baseline)

DB reset:<br>
<code>gradle flywayClean</code>


<h3>Eclipse Plugins</h3>
<ul>
  <li>http://download.eclipse.org/buildship/updates/e45/releases/1.0</li>
  <li>http://www.nodeclipse.org/updates/gradle-ide-pack/</li>
</ul>
![list](https://github.com/gemdat/wiki_pictures/blob/master/gradle/eclipse_installation_details.png)

